# ENGSCI 233 Project: Battle of the Micro:Bits

The music fades. The lights flash. The crowds roar. It's the most anticipated tournament of the year. Rock Paper Scissors.

![Courtesy of Mark Blinch/Reuters](lib/rps_drama.jpg)

Dozens will reach for the top - but [there can be only one](https://www.youtube.com/watch?v=sqcLjcSloXs).

## Lab Overview
In this lab you'll be wielding the skills you've developed over the course to create an interactive experience™.
You will need to draw from previous modules and labs in order to complete this task successfully.

You will be implementing the age-old classic ~~DOOM~~ Rock-Paper-Scissors on your micro:bit.

You have been provided with:

-   this document
-   a testing framework
-   some implementation

You will be maintaining your codebase in a git repository and submitting it, as well as participating in a practical assessment.

## Practice Tasks
_These practice tasks are not assessed directly, but will be required in order to have a functioning system._

### Task 0: Forking this Repository
[Follow these instructions](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).
ENSURE YOUR FORKS ARE PRIVATE.

### Task 1: Selecting an Opponent
**Write code to select an opponent.** Players are identified by a 2-digit number from 0 to 99. IDs with value lower than 10 are padded with a leading zero.

Consider using a code structure like:

```python
def choose_opponent():
  set <data structure> opponent_id to some_initial_value
  loop:
    display opponent_id
    sleep briefly
    if button_a is pressed:
      iterate opponent_id
    if button_b is pressed:
      break loop
  end loop
  return opponent_id
```

### Testing our Function
A testing framework is provided to you in [`test_framework.py`](test_framework.py). This depends on modules defined in [`testing_modules`](testing_modules). We have implemented these modules as mocking modules - imitations of the behaviour on the micro:bit. Recall the use of `pytest` in previous labs.

We can test our `choose_opponent` function in the testing suite by defining a test for it. We have provided this test for you.

#### Things to note when testing with buttons

-   The button mocking class needs to mock the button inputs - this is done by queueing button presses with the mocking function `press`.
-   We set up our button presses before running our function to test since we cannot (in our implementation) inject button presses while the function is running.
-   We use the sleep method to advance our button input history, i.e. simulated time.
-   Used some exotic module that wasn't implemented? Contribute to the repository - open a pull request.
-   Stuck with some exotic module in the micro:python library? Check the issues page, and if no-one has reported it before, raise an issue.

### Task 2: Selecting a Play
**Write code to select a play** - one of Rock, Paper, or Scissors - to send to the opponent.

_Note_: This can be a manual selection at the moment. If you choose this route, consider using a looping logic similar to the opponent selection above. _We highly recommend this while you haven't thought up an AI strategy yet._
You can also opt to go "all rock all the time".

Ideally, this should return a value like `b'R'`, `b'P'` or `b'S'`, so you can easily construct a bytes message to send.

Remember to write and run a test for your code.

### Task 3: Sending a Message
**Write code to send a message with a play and your ID via the radio.**

Have a look at [the documentation](https://microbit-micropython.readthedocs.io/) and locate an appropriate command.
Remember that we are sending bytes objects.

Ensure the message you are sending is compliant with the [Communication Protocol](#communication-protocol).

You can test your code with the given test in the test framework.

#### Notes on testing with the radio
Examine the documentation for the radio module if you intend to implement tests.

Notice that there are the following additional helper functions and variables:

*   `state`: This is the queue incoming messages. This corresponds to messages that would be passively received by the radio.
*   `out_queue`: This the stack of outgoing messages. This corresponds to messages sent by the radio.
*   `push_message()`: This puts messages into `state`, i.e. acting as "receiving" messages.
*   `get_last_out()`: This pops the last message sent in the `out_queue`. We can use this to check messages that we send.

### Task 4: Sending an Acknowledgement
**Write code to send an acknowledgement message.**

This should be similar to the code you have written for sending an arbitrary play, but with a fixed contents value.

You can test your code with the given test in the test framework.

### Task 5: Parsing a Message
**Write code to parse an incoming message according to the communication protocol.**

It should:

*   Validate that the message is of a valid length
*   Validate the sender and receiver are correct
*   Respond appropriately (acknowledgement messages)
*   Return the correct play values for game resolution

We can use any of the receive functions - just note that we are expecting and parsing bytes objects.
One thing to note about bytes objects is that they operate under the buffer protocol. For our purposes, this means we have to watch out for how we index into bytes objects, as they behave subtly differently to string objects. For example:

```python
normal_string = 'abcdefg'
bytes_string = b'abcdefg'

normal_string[0:4]    # returns 'abcd'
normal_string[3]      # returns 'd'

bytes_string[0:4]     # returns b'abcd'
bytes_string[3]       # returns 100
bytes_string[3:4]     # returns b'd'
```

Once you have implemented this, you should write and run a test for this code.

### Task 6: Putting it Together
**Examine the main control loop, and complete the implementation.** Pseudocode for it is given below.

```python
def main():
  initialise <int> score to 0
  initialise <int> round_number to 0
  initialise <list> memory
  get <bytes> opponentID from buttons
  loop for ever:
    get <bytes> choice from buttons
    send choice via radio
    initialise <bool> acknowledged to False
    initialise <bool> resolved to False
    loop until acknowledged and resolved
      get <bytes> message from radio
      if message is play:
        store message in memory
        set <int> result from resolve of choice and message
        update score from result
        set resolved to True
        display result and score
      if message is acknowledgement:
        set acknowledged to True
      if not acknowledged:
        send choice via radio
    end loop
    update round_number
  end loop
```

The resolving and scoring functions are already provided to you.

At this point, you should be able to **flash the code onto the micro:bit** for a test run. Make sure you select the minify code setting as specified on [this page](https://codewith.mu/en/howto/1.0/microbit_settings), to save space on the micro:bit.

## Note
Memory Errors? Follow [these instructions](https://codewith.mu/en/howto/1.0/microbit_settings) for minifying the code as it is flashed.

For this to work well, you should use the `#` syntax for comments and docstrings, in place of triple quotes (`'''`). In the case of docstrings, you can use the style:

```python
def foo(args):
  # ''' Function Summary
  #
  # Field:
  # ------
  # Field Contents
  #
  # '''

  function_contents
```
(Tip: write your docstring out normally, then comment with `CTRL+K`, or `CMD+K` on Mac)

If this is still insufficient, you can shorten your code by replacing the import statements with `from microbit import *` and the like. However, if you do this, be VERY careful that you have not named any of your own functions or variables in ways that conflict with names in the libraries you import, or unexpected behaviour will result. You might also wish to choose shorter variable and function names, but be sure to keep them understandable to earn your code style marks.

## Project Task
Artificial intelligence is the future. In the years to come, some computer scientist will find a way to automate you out of your software engineering/consulting/product design job that you worked oh-so-hard for. And then you'll need to join them. As preparation for this, you will need skills. Skills like coding an AI to play rock-paper-scissors. Trust us, you'll need this someday.

**Implement an automatic rock-paper-scissors playing function that selects your rock-paper-scissors play for you.** This can (but does not need to) be an information-driven _"intelligent"_ algorithm, or it could be `random.choice`. The specification of the functionality you need to implement is provided below.

### Hint
You should be able to achieve 'intelligence' by:

*   Storing your opponents moves in some data structure
*   Making a choice based on those stored moves (i.e. in the `choose_play` function)

## Specification
### Communication Protocol
Your device will need to send messages via the radio with the following message structure:

`|OPPONENT_ID|MY_ID|CONTENTS|ROUND_NUMBER|`

where `CONTENTS` is one of `R`, `P`, `S` or `X` (detailed [below](#Acknowledgement)). This message must be sent as [byte strings](https://docs.python.org/3/library/stdtypes.html#bytes). An example message would be:

`b'0060R2'`

which would denote a choice of `ROCK` by micro:bit `60` for round `2` sent to micro:bit `00`.

**Note**: The round number can be converted from an integer to this representation by using `bytes(str(number), 'UTF-8')`

#### Acknowledgement
Messages that contain a play (`R`, `P` or `S`) received by a device must be acknowledged with the `CONTENTS` value of `X`. For example, if the device `50` receives a play from device `49` for round `7`, the message sent to acknowledge this would be:

`b'4950X7'`

### Functionality
You must be able to set and record the ID of your current opponent on the device without re-flashing.

You must be able to send a message following the communication protocol.

You must acknowledge messages sent to you appropriately.

You must ensure that your message is delivered by utilising the communication protocol.

You must be able to send one individual play at the press of a button. This play must be automatically made.

## Assessment Tournament
A portion of this lab's marks will come from a where you will pit your AI against the AIs of other prospective champions of Rock-Paper-Scissors. **This will be played in the lab sessions on Wednesday 5 June 2019**.


### Structure
The tournament will be played as double-elimination knockout with a loser's bracket.

Each pair will participate in an initial round. Winners proceed into the winners' bracket; losers to the losers' bracket. Games in the winners' bracket proceed as a standard single elimination tournament; losers of winners' brackets games drop into the losers' bracket, which plays as single elimination. The final will be played between the winner of each of the winners' and losers' bracket, with a repeat match if the winners' bracket winner loses in the first final's match (for proper double elimination).

### Ruleset
Games will be played as first to 9, or highest score after 5 minutes. In the case of a stalemate, a coinflip will be used to discern a winner.

You will be randomly seeded within your lab session. A bracket will be visible on the front screen, with matches assigned as the bracket progresses.

Matches will be played at various stations in the lab space. Matches will be assigned stations, and you should go to them to play your games. Report the results to a lecturer or TA as you conclude your games, in order to have your results recorded, and proceed in the tournament.

**_If you cannot attend the lab session for some legitimate reason, please notify the course intructor as soon as possible._**

## Submission Instructions
_Submissions are due by 10pm Tuesday 4th June 2019._

For this project, submit:

-   `project_repository.txt` that points to your private git repository. Add the user `ddempsey1234` with Read permissions to this repository.

Your repository should contain (at least):

-   `main.py`: The file you flash onto the microbit
-   `README.md`: An appropriate README file

It would be preferable to not include this document in your final submission.

### Rubric

| Criteria                 | Mark      | Criterion                                       |
|--------------------------|-----------|-------------------------------------------------|
| Tournament Participation | 3         | Has demonstrably working AI implementation |
|                          | 2         | Participates in tournament                         |
|                          | 0         | Not present for assessment                      |
| Tournament Results       | Up to 1.5 | See [Tournament Breakdown](#tournament-breakdown) below |
| Style                    | 0.5       | Efficient implementation, maintainable codebase      |
|                          | 0         | Significant inefficiencies, unmaintainable code |

#### Tournament Breakdown
Marks will be allocated based on the differential of wins between you and the champion:

| # Wins fewer than Champion | Mark |
|----------------------------|------|
| 0 (Champion)               | 1.5  |
| 1                          | 1.25 |
| 2                          | 1    |
| 3                          | 0.75 |
| 4                          | 0.5  |
| 5                          | 0.25 |
| 6+                         | 0    |

#### Marking Notes
You may notice that this project can generate more than 3.5 marks. The additional marks of this project will be honoured as plussage for the lab section of this course.
