import microbit
import utime
import radio

# GLOBAL
MYID = b'69' # change this to be the same as your group number from Canvas
RPS = (b'R', b'P', b'S')

def choose_opponent():
    # """ Return the opponent id from button presses
    #
    # Returns
    # -------
    # bytes
    #
    # """
    return b''

def choose_play():
    # """ Returns the play selected from button presses
    #
    # Returns
    # -------
    # bytes
    # """
    return b''

def send_choice(opponent_id, play, round_number):
    # """ Sends a message via the radio """
    pass

def send_acknowledgement(opponent_id, round_number):
    # """ Sends an acknowledgement message via the radio """
    pass

def parse_message():
    # """ Receive and parse the next valid message
    #
    # Returns
    # -------
    # bytes
    # """
    return b''

def resolve(my, opp):
    # """ Returns the outcome of a rock-paper-scissors match
    # Also displays the result
    #
    # Parameters
    # ----------
    # my  : bytes
    #     The choice of rock/paper/scissors that this micro:bit made
    # opp : bytes
    #     The choice of rock/paper/scissors that the opponent micro:bit made
    #
    # Returns
    # -------
    # int :
    #     Numerical value for the outcome as listed below
    #      0: Loss/Draw
    #     +1: Win
    #
    # Notes
    # -----
    # Input parameters should be one of b'R', b'P', b'S'
    #
    # Examples
    # --------
    # solve(b'R', b'P') returns 0 (Loss)
    # solve(b'R', b'S') returns 1 (Win)
    # solve(b'R', b'R') returns 0 (Draw)
    #
    # """
    result = RPS.index(my) - RPS.index(opp)
    resolution = [0, 1, 0][result]
    face = [microbit.Image.ASLEEP, microbit.Image.HAPPY, microbit.Image.SAD][result]
    microbit.display.show(face)
    microbit.sleep(333)
    return resolution

def display_score(score, times=3):
    # """ Flashes the score on the display
    #
    # Parameters
    # ----------
    # score : int
    #     The current score
    # times : int
    #     Number of times to flash
    #
    # Returns
    # -------
    # None
    #
    # Notes
    # -----
    # The variable score should be between 0 and 9, inclusive. This is due to a limitation of showing
    # images on the micro:bit display that require more than one screen's worth of pixels to show.
    # """
    screen_off = microbit.Image(':'.join(['0'*5]*5))
    microbit.display.show([screen_off, str(score)]*times, delay=100)

def main():
    # """ Main control loop"""
    # set up the radio
    radio.config(power=6, queue=20)
    radio.on()
    # initialise score, round number and memory
    score = 0
    round_number = 0
    memory = []
    # select an opponent
    opponent_id = choose_opponent()
    # loop forever
    while True:
        # get a play from the buttons
        choice = choose_play()
        # send choice
        send_choice(opponent_id, choice, round_number)
        # TODO: record when the message was sent
        send_time = 0
        acknowledged, resolved = (False, False)
        # passive waiting display
        microbit.display.show(microbit.Image.ALL_CLOCKS, wait=False, loop=True)
        while not acknowledged and not resolved:
            # get a message from the radio
            message = parse_message()
            # TODO: if is a play
            if False:
                # resolve the match
                result = resolve(choice, message)
                # update score

                # microbit.display result and score
                microbit.display_score(score)
            # TODO: if is acknowledgement
            if False:
                pass
            # if not acknowledged, resend message every 3s
            if not acknowledged and utime.ticks_diff(utime.ticks_ms, send_time) > 3000:
                send_choice(opponent_id, choice, round_number)
                send_time = utime.ticks_ms()
        # TODO: Update round number

if __name__ == "__main__":
    main()